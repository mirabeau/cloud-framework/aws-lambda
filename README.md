AWS Lambda
=========
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

This role creates an S3 bucket where lamda function code can be uploaded before deployment.
On top of that, the role functions as a wrapper to deploy multiple lambda roles at once.
Default all lambda functions that are part of the MCF repository will be deployed, but it can be configured using group_vars.

A test playbook has been supplied which rolls out the S3 bucket and all lambda functions as part of MCF.
You can run this playbook using the following command:
```bash
ansible-playbook aws-lambda/tests/test.yml --inventory aws-lambda/tests/inventory.yml
```

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x  
Pip 18.x or higher (Python 2.7)

Required python modules:
* boto
* boto3
* awscli
* docker

Dependencies
------------
- aws-utils

Role Variables
--------------
### _Internal_
```yaml
lambda_stack_prefix: "lambda"
```
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```
Role Defaults
--------------
```yaml
create_changeset     : True
debug                : False
cloudformation_tags  : {}
tag_prefix           : "mcf"
lambda_log_expiration: 7
 
aws_lambda_params:
  create_s3         : True
  create_changeset  : "{{ create_changeset }}"
  debug             : "{{ debug }}"
  environment_abbr  : "{{ account_abbr }}"
  lambda_bucket_name: "{{ account_name }}-{{ aws_region }}-lambda"
  lambda_roles:
    - aws-lambda-childtagger
    - aws-lambda-cfn-secretsprovider
    - aws-lambda-ebs-snapshots
    - aws-lambda-ecs-instancedrainer
    - aws-lambda-ecs-monitor
    - aws-lambda-getami
    - aws-lambda-getondemandprice
    - aws-lambda-route53manager
    - aws-lambda-setuniquehostname
    - aws-lambda-sandman
    - aws-lambda-updatestack
    - aws-lambda-asgdetachinstance
    - aws-lambda-asgupdatecounts
```
Example Playbooks
-----------------
Rollout the aws-lambda files with defaults
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    tag_prefix      : "mcf"
    aws_region      : "eu-west-1"
    owner           : "a-company"
    account_name    : "a-com-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"

  roles:
    - aws-lambda
```
License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
